#Importaci�n de librerias y funciones
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
from scipy.special import factorial2
from numpy import sin, deg2rad
  
def f_ang(theta): #Definimos una funci�n para obtener la Ecuaci�n 2:
    serie = np.array([(factorial2(2 * n - 1) * (sin(deg2rad(theta/2))**(2 * n))/factorial2(2 * n) )**2 for n in range(1,151)])
    return 1 + np.sum(serie)

def omega2(g, theta, M, m, R, u, l, L, r): #Definimos una funci�n para obtener la Ecuacion 4:
    
    # g(gravedad)
    # theta(�ngulo de oscilacion del pendulo)
    # M(Masa fija en extremo inferior de varilla)
    # m(masa que puede moverse  arriba o abajo de varilla para cambiar frecuencia de oscilaci�n)
    # R(distancia del eje al centro de masa de M )
    # u(masa de varilla)
    # l(longitud de varilla del eje al extremo inferior)
    # L(Longitud de varilla del eje al extremo superior)
    # r(distancia del eje al centro de masa de m )
    
    Mp = M/m    #masa no dimensional baja corregida
    up = u/m    #masa de la varilla corregida
    #Ecuaci�n 5:
    a_0 = (g/(f_ang(theta)**2)) * ((Mp * R) - (up * (l - L)/2))/((Mp * (R**2)) + (up * ((L**2)+(l**2)-(l*L))/3))
    #Ecuaci�n 6:
    b_2 = - 1/((Mp * (R**2)) + (up *( (L**2) + (l**2) - (l*L))/3))
    omega2 = ((a_0 + b_2 * g/(f_ang(theta)**2))/(1 - b_2*(r**2)))
    return omega2

rs = np.arange(36 , 218, 12) #"rs" Array de los valores que puede tomar r: [40, 218].
valores = [9800.0, 50.0, 31.0, 7.1, 51.0, 3.59, 138.0, 50.0]   # "valores" lista con las variables necesarias en omega2
print(rs)
fig = plt.figure(figsize = (9, 12))     #M�rgen en donde se va a dibujar nuestra funcion y sliders
ax1 = plt.axes((0.15, 0.6, 0.75, 0.3))  #Dimensiones para graficar omega2.

#Se gr�fica omega2 con los valores de la lista iniciales

ax1.plot(rs,omega2( 9800.0, 50.0, 31.0, 7.1, 51.0, 3.59, 138.0, 50.0,rs), '-ok', color = '#804000', label = 'Metr�nomo de Beethoven')
ax1.set_title("Metr�nomo de Beethoven" ,  fontdict={'family': 'serif', 'color' : 'red', 'weight': 'bold', 'size': 25})
ax1.set_xlabel("r (mm)",fontdict = {'family': 'serif', 'color' : 'darkblue', 'weight': 'bold', 'size': 15})
ax1.set_ylabel("$\Omega^2 (bpm)$", fontdict = {'family': 'serif', 'color' : 'darkblue', 'weight': 'bold', 'size': 18} )
ax1.set_ylim(0, 220)
ax1.axhline(120, color = 'blue', ls = '--')
ax1.axhline(108, color = 'blue', ls = '--')
ax1.legend()
ax1.grid()

n = 0                #"n" variable para indicar la ubicación de la figura.
interfaz = []        #"interfaz" lista de la ubicaión de (9) para los Sliders.
for i in range(9):   #Bucle para especificar los axes de los Sliders.
    if i == 0:
        interfaz.append(plt.axes((0.3, round(0.1 + n,2), 0.4, 0.035)))
    else:
        interfaz.append(plt.axes((0.13, round(0.1 + n,2), 0.8, 0.035)))
    n += 0.045

barras = []          #"barras" lista para los (9) Sliders.  
label = ['g ($mm/s^2$)', 'thetha (°)', 'M (gr)','m (gr)', 'R (mm)', 'u (gr)', 'l (mm)', 'L (mm)']    #"label" lista de (9) variables.
valmax = [9800.0, 52.0, 50.0, 15.0, 40.0, 5.0, 218, 35.0]                                            #"valmax" lista de valores máximos. 
valmin = [7000.0, 0.0, 30.0, 5.0, 9.0, 3.0, 100, 20.0]                                               #"valmin" lista de valores mínimos.   
valstep = [100.0, 2.0, 4.0, 1.0, 2.0, 0.5, 4.0, 2.5]                                                 #"valstep" lista de varaciones de los Sliders.   
colores = ['Green', 'Cyan', 'Chocolate', 'BlueViolet', 'Pink', 'LightGreen', 'Tan', 'DarkGrey']      #"valstep" lista con colores para los Sliders.    

for i in range(1,9):   #Bucle para crear los Sliders.
    barras.append(Slider(ax = interfaz[i], label = label[i - 1],valmin = valmin[i - 1], 
                         valmax = valmax[i - 1], valinit = valores[i -1], valstep = valstep[i - 1],
                         orientation = 'horizontal', color = colores[i-1]))
    barras[i-1].label.set_size(14)

def actualizar(valor):  #Funci�n para crear una gr�fica a partir de los valores variados en los Sliders.
    for i in range(8):  #Bucle para actualizar la lista "valores".
        valores[i] = barras[i].val
    ax1.clear()
    #Se plotea nuevamente la grafica con el valor correspondiente al slider cambiado
    ax1.plot(rs,omega2( 9800.0, 50.0, 31.0, 7.1, 51.0, 3.59, 138.0, 50.0, rs), 
             '-ok', label = 'Metr�nomo de Beethoven', color = '#804000')
    ax1.plot(rs,omega2( valores[0], valores[1], valores[2], valores[3], valores[4], valores[5], valores[6], valores[7], rs), 
             '-ok', label = 'Metr�nomo con par�metros variados', color = '#998ec3')
    ax1.set_title("Metr�nomo de Beethoven" ,  fontdict = {'family': 'serif', 'color' : 'red', 'weight': 'bold', 'size': 25})
    ax1.set_xlabel("r (mm)", fontdict = {'family': 'serif', 'color' : 'darkblue', 'weight': 'bold', 'size': 15})
    ax1.set_ylabel("$\Omega^2 (bpm)$", fontdict={'family': 'serif', 'color' : 'darkblue', 'weight': 'bold', 'size': 18})
    ax1.set_ylim(0, 220)
    ax1.axhline(120, color = 'blue', ls = '--')
    ax1.axhline(108, color = 'blue', ls = '--')
    ax1.legend()
    ax1.grid()
    fig.canvas.draw_idle()

#Se actualiza cada barra o slider 

barras[0].on_changed(actualizar)
barras[1].on_changed(actualizar)
barras[2].on_changed(actualizar)
barras[3].on_changed(actualizar)
barras[4].on_changed(actualizar)
barras[5].on_changed(actualizar)
barras[6].on_changed(actualizar)
barras[7].on_changed(actualizar)

rest = Button(ax = interfaz[0], label = 'Reiniciar') #Se establece un bot�n de reiniciar a los valores iniciales
def reiniciar(evento):                               #Funcion para reiniciar todos los sliders.
    for i in range(8):
        barras[i].reset()
rest.on_clicked(reiniciar)

plt.show()
